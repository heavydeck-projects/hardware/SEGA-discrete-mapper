% Constraint Logic Programming
:- use_module(library(clpb)).    % Boolean constraints

main :-
    format("--- Program start ---\n"),
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Address mask sub-circuit %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %Mask is the logical-nand of address bits [A2, A15]
    Mask = (
                  A2  * A3  * A4  * A5  * A6  * A7 *
        A8 * A9 * A10 * A11 * A12 * A13 * A14 * A15
    ),
    NMask = ~Mask,
    %Here it is implemented using two 8-input NAND gates and an inverter.
    %The output of this new Mask is NEGATED hence the 'N' Prefix
    %  - 2x 8-input NAND gate (74HC30)
    %  - 1x Inverter (from 74HC04)
    %
    %                 6                +---+
    %  [A2,A7] -------/----------------|   |
    %  True (Vcc) ---------------------| & |o----> NMask_new
    %                               +--|   |
    %            8 +---+    +---+   |  +---+
    %  [A8,A15] -/-| & |o---| 1 |o--+
    %              +---+    +---+
    %
    NMask_new = (
        %Outer nand (8-input)
        ~(
            A2 *
            A3 *
            A4 *
            A5 *
            
            A6 *
            A7 *
            1  *
            (
                %Inverter
                ~(
                    %Inner nand (8-input)
                    ~(
                        A8  *
                        A9  *
                        A10 *
                        A11 *
                        
                        A12 *
                        A13 *
                        A14 *
                        A15
                    )
                )
            )
        )
    ),
    Mask_new = ~NMask_new,
    %Verify equivalence of Mask and Mask_new
    taut(Mask  =:= Mask_new, 1),
    taut(NMask =:= NMask_new, 1),
    format("Mask subcircuit OK\n"),
    
    %%%%%%%%%%%%%%%%%%%%%%%
    %%% Slot1/2 Address %%%
    %%%%%%%%%%%%%%%%%%%%%%%
    
    %These two come from the Address decoder PLD are will be used to
    %emulate the old mapper behaviour.
    Slot1_address = Mask * ~A0 * A1,
    Slot2_address = Mask *  A0 * A1,
    
    %Validate against new mask (not really needed)
    taut(Slot1_address =:= Mask_new * ~A0 * A1, 1),
    taut(Slot2_address =:= Mask_new *  A0 * A1, 1),
    
    %%%%%%%%%%%%%%%%%%%%%%
    %%% Slot1/2 Enable %%%
    %%%%%%%%%%%%%%%%%%%%%%
    
    Slot1_enable = A14  * ~A15,
    Slot2_enable = ~A14 *  A15,
    NSlot1_enable = ~Slot1_enable,
    NSlot2_enable = ~Slot2_enable,
    
    
    %Implemented using 2-input NAND (74HC00) and inverter (74HC04)
    %
    %                   +---+
    %   A14 ------------|   |
    %          +---+    | & |o--> NSlot1_enable_new
    %   A15 ---| 1 |o---|   |
    %          +---+    +---+
    %
    NSlot1_enable_new = (
        %Nand gate
        ~(
            %Inverter
            ~(
                A15
            ) *
            A14
        )
    ),
    Slot1_enable_new = ~NSlot1_enable_new,
    
    %Implemented using 2-input NAND (74HC00) and inverter (74HC04)
    %
    %          +---+    +---+
    %   A14 ---| 1 |o---|   |
    %          +---+    | & |o--> NSlot2_enable_new
    %   A15 ------------|   |
    %                   +---+
    %
    NSlot2_enable_new = (
        %Nand gate
        ~(
            %Inverter
            ~(
                A14
            ) *
            A15
        )
    ),
    Slot2_enable_new = ~NSlot2_enable_new,
    
    %Test equality
    taut(Slot1_enable  =:= Slot1_enable_new, 1),
    taut(Slot2_enable  =:= Slot2_enable_new, 1),
    taut(NSlot1_enable =:= NSlot1_enable_new, 1),
    taut(NSlot2_enable =:= NSlot2_enable_new, 1),
    format("Slot1/2 enable OK\n"),
    
    %Make sure these are never active at the same time
    taut(Slot1_enable * Slot2_enable, 0),
    format("Slot1/2 non-overlapping OK\n"),
    
    
    
    %%%%%%%%%%%
    %%% END %%%
    %%%%%%%%%%%
    
    format("All OK.\n"),
    format("--- Program end ---\n").
    
:- main.
