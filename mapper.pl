
:- use_module(library(clpb)).    % Boolean constraints

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Declaration of logic gates (from 74-series circuits %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 2-input nand gate, from 74HC00
nand_2(Q, P0, P1):-
	sat(Q =:= ~(P0 * P1)).

% 8-input nand gate, from 74HC30
nand_8(Q, P0, P1, P2, P3, P4, P5, P6, P7):-
	sat(Q =:= ~(P0 * P1 * P2 * P3 * P4 * P5 * P6 * P7)).

% inverter, from 74HC04
not_1(Q, P):-
	sat(Q =:= ~P).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Declaration of the original PLD-Based mapper %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Address mask PLD %%%

%slot1_addr
%  Out: Positive signal, true if A==0xFFFE
%   in: [A0, A15]
%Original PLD code:
%SLOT1_ADDR =  (!A0) & A1 & A2 & A3 & A4 & A5 & A6 & A7 & A8 & A9 & A10 & A11 & A12 & A13 & A14 & A15 ;
slot1_addr(Q, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15) :-
	sat(Q =:= (~A0) * A1 * A2 * A3 * A4 * A5 * A6 * A7 * A8 * A9 * A10 * A11 * A12 * A13 * A14 * A15).

%slot2_addr
%  Out: Positive signal, true if A==0xFFFF
%   in: [A0, A15]
%Original PLD code:
%SLOT2_ADDR =  A0 & A1 & A2 & A3 & A4 & A5 & A6 & A7 & A8 & A9 & A10 & A11 & A12 & A13 & A14 & A15 ;
slot2_addr(Q, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15) :-
	sat(Q =:= A0 * A1 * A2 * A3 * A4 * A5 * A6 * A7 * A8 * A9 * A10 * A11 * A12 * A13 * A14 * A15).

%%% Mapper PLD %%%

%n_slot1_enable
%  Out: Negative signal, slot 1 latch !enable
%   In: A14, A15
%Original PLD:
%SLOT1_OE = ( A14) & (!A15) ;
n_slot1_enable(Q, A14, A15) :-
	sat(Q =:= ~(A14 * (~A15))).

%n_slot2_enable
%  Out: Negative signal, slot 2 latch !enable
%   In: A14, A15
%Original PLD:
%SLOT2_OE = (!A14) & ( A15) ;
n_slot2_enable(Q, A14, A15) :-
	sat(Q =:= ~(A15 * (~A14))).

%slot1_latch
%  Out: Positive signal, slot 1 latch load.
%   In: slot1_addr, MREQ, WR
%Original PLD:
%SLOT1_LATCH = SLOT1_ADDR & MREQ & WR ;
slot1_latch(Q, S1Address, MREQ, WR) :-
	sat(Q =:= S1Address * MREQ * WR).

%slot2_latch
%  Out: Positive signal, slot 1 latch load.
%   In: slot2_addr, MREQ, WR
%Original PLD:
%SLOT2_LATCH = SLOT2_ADDR & MREQ & WR ;
slot1_latch(Q, S2Address, MREQ, WR) :-
	sat(Q =:= S2Address * MREQ * WR).

%%% The full mapper %%%

mapper(
	%Outputs
	N_Slot2_enable, Slot2_latch, N_Slot1_enable, Slot1_latch,
	%Inputs
	% - Address bus
	A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15,
	% - CPU Signals
	N_MREQ, N_WR
	):-
		%SLOT2 circuitry
		slot2_addr(IS_SL2_ADDRESS, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15),
		n_slot2_enable(N_Slot2_enable, A14, A15),
		slot2_latch(Slot2_latch, IS_SL2_ADDRESS, ~N_MREQ, ~N_WR),
		%SLOT1 circuitry
		slot1_addr(IS_SL1_ADDRESS, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15),
		n_slot1_enable(N_Slot1_enable, A14, A15),
		slot1_latch(Slot1_latch, IS_SL1_ADDRESS, ~N_MREQ, ~N_WR)
		.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Declaration of the discrete-logic mapper %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

